#!/bin/bash

ls | grep run_ > runList.txt
input=runList.txt
while IFS= read -r line
do
    if [ -f $line"/fluka.log.gz" ]; then
        echo $line
        echo "FLUKA log file found"
        gzip -d $line/fluka.log.gz
        awk '{print $14}' $line/fluka.log | tail --lines=2 | head --lines=1 >> flukaLogRes.dat
        gzip $line/fluka.log
    fi
done < "$input"
rm runList.txt
