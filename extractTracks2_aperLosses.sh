#! /bin/bash

tail --lines=+2 aperture_losses.dat > temp_hits.dat

if [ -s "temp_hits.dat" ]
then
    awk '{print $6}' temp_hits.dat > temp_hits_np.dat 
    input=temp_hits_np.dat
    while IFS= read -r line
    do
        echo "$line"
        awk '$1 == '"$line"' {print $0}' tracks2.dat > tracksExtracted"$line".dat
    done < "$input"
fi




