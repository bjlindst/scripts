#!/bin/bash

Help()
{
    echo "This script prepares the input files for a sixtrack-FLUKA run."
    echo "It requires collimator settings file, b1 or b4, H or V and start position, as input arguments."
    echo
    echo "Run the script as prepareRun.sh arg1 arg2 arg3 arg4 arg5,"
    echo "where arg1 is the full path to the collDB file, arg2 is 'b1' or 'b4', arg3 is 'H' or 'V',"
    echo "arg4 is 'STARTPOSITION' in upper case corresponding to the MADX name, arg5 is a unique identifier,"
    echo "arg6 is normalized emittance [µm.rad], arg7 is pc [GeV], arg8 is xing angle [µrad], arg9 is beta* [cm],"
    echo "arg10 the requested impact parameter [µm], arg11 the collimators for touchMap [all/IR7/TCT/TCS/IR3],"
    echo "arg12 the number of turns and arg13 the number of particle pairs."
    echo "arg14 [optional] is the full path to a folder containing a fort.2, fort3.limi and twiss file."
    echo "arg15 [optional] should be 1 if aperture offsets are to be taken into account."
    echo "arg16 [optional] is the full path to a prototypes.lbp file."
    echo "arg17 [optional] is the name of the twiss file in the folder specified by arg14." 
    echo
    echo "Note that if arg15 and/or arg16 is to be used, arg14 must also be defined."
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run prepareRun.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "$3" ]
then
    echo "argument 3 not provided. Run prepareRun.sh -h for help."
    exit;
fi
if [ -z "$4" ]
then
    echo "argument 4 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "$5" ]
then
    echo "argument 5 not provided. Run prepareRun.sh -h for help."
    exit;
fi       
if [ -z "$6" ]
then
    echo "argument 6 not provided. Run prepareRun.sh -h for help."
    exit;
fi  
if [ -z "$7" ]
then
    echo "argument 7 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "$8" ]
then
    echo "argument 8 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "$9" ]
then
    echo "argument 9 not provided. Run prepareRun.sh -h for help."
    exit;
fi    
if [ -z "${10}" ]
then
    echo "argument 10 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "${11}" ]
then
    echo "argument 11 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "${12}" ]
then
    echo "argument 12 not provided. Run prepareRun.sh -h for help."
    exit;
fi   
if [ -z "${13}" ]
then
    echo "argument 13 not provided. Run prepareRun.sh -h for help."
    exit;
fi   


# Log the input command
date >> preparationLog.txt
echo $@ >> preparationLog.txt



# Global constants
PMASS=0.938

rm -rf sixtrackK2run_$5
rm -rf clean_input$5$3
rm -rf runSingle$5$3
rm -rf raw_input
rm -rf preprocessed_input
mkdir sixtrackK2run_$5
cd sixtrackK2run_$5
if [ -z "${14}" ]
then
    echo "Arg 14 not found - using default fort/twiss files for specified beta*/xing"
    ln -s /afs/cern.ch/work/b/bjlindst/public/sixtrack/batchRuns/templates/HLLHCV1.5/$2/$8muradXing$9cmBetaNoTCLD/* ./
    #ln -s /afs/cern.ch/work/b/bjlindst/public/twissFiles/HLLHCV1.5/$8muradXing$9cmBetaNoTCLD/LHC_$2_$7GeV_thinXing.twiss ./
else
    echo "Arg 14 found - using fort/twiss files in specified folder"
    ln -s ${14}/* ./
    if [ -z "${17}" ]
    then
        echo "Arg 17 not found, twiss file already named correctly"
    else
        echo "Arg 17 found - renaming twiss file to be compatible with the rest of the script"
        ln -s ${17} ./LHC_$2_7000GeV_thinXing.twiss
    fi
fi
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/batchRuns/templates/fort.3 ./
cp $1 ./CollDB_HL.data

# use CollDB file instead of fort.3 settings, necessary for betaStar > 20 cm 
sed -i 's/.USECOLLDB./.FALSE./g' fort.3   # sets to use CollDB
#sed -i 's/.USECOLLDB./.TRUE./g' fort.3   # sets to not use CollDB
# also add the settings to fort.3 
nsigTCP3=$(grep -i 'NSIG_FAM tcp3' CollDB_HL.data | awk '{printf $3}')
nsigTCSG3=$(grep -i 'NSIG_FAM tcsg3' CollDB_HL.data | awk '{printf $3}')
nsigTCSM3=$(grep -i 'NSIG_FAM tcsm3' CollDB_HL.data | awk '{printf $3}')
nsigTCLA3=$(grep -i 'NSIG_FAM tcla3' CollDB_HL.data | awk '{printf $3}')
nsigTCP7=$(grep -i 'NSIG_FAM tcp7' CollDB_HL.data | awk '{printf $3}')
nsigTCSG7=$(grep -i 'NSIG_FAM tcsg7' CollDB_HL.data | awk '{printf $3}')
nsigTCSM7=$(grep -i 'NSIG_FAM tcsm7' CollDB_HL.data | awk '{printf $3}')
nsigTCLA7=$(grep -i 'NSIG_FAM tcla7' CollDB_HL.data | awk '{printf $3}')
nsigTCLP=$(grep -i 'NSIG_FAM tclp' CollDB_HL.data | awk '{printf $3}')
nsigTCLI=$(grep -i 'NSIG_FAM tcli' CollDB_HL.data | awk '{printf $3}')
nsigTCDQ=$(grep -i 'NSIG_FAM tcdq' CollDB_HL.data | awk '{printf $3}')
nsigTCSTCDQ=$(grep -i 'NSIG_FAM tcstcdq' CollDB_HL.data | awk '{printf $3}')
nsigTDI=$(grep -i 'NSIG_FAM tdi' CollDB_HL.data | awk '{printf $3}')
nsigTCTH1=$(grep -i 'NSIG_FAM tcth1' CollDB_HL.data | awk '{printf $3}')
nsigTCTH2=$(grep -i 'NSIG_FAM tcth2' CollDB_HL.data | awk '{printf $3}')
nsigTCTH5=$(grep -i 'NSIG_FAM tcth5' CollDB_HL.data | awk '{printf $3}')
nsigTCTH8=$(grep -i 'NSIG_FAM tcth8' CollDB_HL.data | awk '{printf $3}')
nsigTCTV1=$(grep -i 'NSIG_FAM tctv1' CollDB_HL.data | awk '{printf $3}')
nsigTCTV2=$(grep -i 'NSIG_FAM tctv2' CollDB_HL.data | awk '{printf $3}')
nsigTCTV5=$(grep -i 'NSIG_FAM tctv5' CollDB_HL.data | awk '{printf $3}')
nsigTCTV8=$(grep -i 'NSIG_FAM tctv8' CollDB_HL.data | awk '{printf $3}')
nsigTCXRP=$(grep -i 'NSIG_FAM tcxrp' CollDB_HL.data | awk '{printf $3}')
nsigTCRYO=$(grep -i 'NSIG_FAM tcryo' CollDB_HL.data | awk '{printf $3}')    
if [ -z "$nsigTCP3" ];
then
    nsigTCP3=999
fi
if [ -z "$nsigTCSG3" ];
then
    nsigTCSG3=999
fi
if [ -z "$nsigTCSM3" ];
then
    nsigTCSM3=999
fi
if [ -z "$nsigTCLA3" ];
then
    nsigTCLA3=999
fi
if [ -z "$nsigTCP7" ];
then
    nsigTCP7=999
fi
if [ -z "$nsigTCSG7" ];
then
    nsigTCSG7=999
fi
if [ -z "$nsigTCSM7" ];
then
    nsigTCSM7=999
fi
if [ -z "$nsigTCLA7" ];
then
    nsigTCLA7=999
fi
if [ -z "$nsigTCLP" ];
then
    nsigTCLP=999
fi
if [ -z "$nsigTCLI" ];
then
    nsigTCLI=999
fi
if [ -z "$nsigTCDQ" ];
then
    nsigTCDQ=999
fi
if [ -z "$nsigTCSTCDQ" ];
then
    nsigTCSTCDQ=999
fi
if [ -z "$nsigTDI" ];
then
    nsigTDI=999
fi
if [ -z "$nsigTCTH1" ];
then
    nsigTCTH1=999
fi
if [ -z "$nsigTCTH2" ];
then
    nsigTCTH2=999
fi
if [ -z "$nsigTCTH5" ];
then
    nsigTCTH5=999
fi
if [ -z "$nsigTCTH8" ];
then
    nsigTCTH8=999
fi
if [ -z "$nsigTCTV1" ];
then
    nsigTCTV1=999
fi
if [ -z "$nsigTCTV2" ];
then
    nsigTCTV2=999
fi
if [ -z "$nsigTCTV5" ];
then
    nsigTCTV5=999
fi
if [ -z "$nsigTCTV8" ];
then
    nsigTCTV8=999
fi
if [ -z "$nsigTCXRP" ];
then
    nsigTCXRP=999
fi
if [ -z "$nsigTCRYO" ];
then
    nsigTCRYO=999
fi
echo "TCP3 is "$nsigTCP3" sigma"
echo "TCSG3 is "$nsigTCSG3" sigma"   
echo "TCSM3 is "$nsigTCSM3" sigma"   
echo "TCLA3 is "$nsigTCLA3" sigma"     
echo "TCP7 is "$nsigTCP7" sigma"   
echo "TCSG7 is "$nsigTCSG7" sigma"      
echo "TCSM7 is "$nsigTCSM7" sigma"      
echo "TCLA7 is "$nsigTCLA7" sigma"       
echo "TCLP is "$nsigTCLP" sigma"        
echo "TCLI is "$nsigTCLI" sigma"       
echo "TCDQ is "$nsigTCDQ" sigma" 
echo "TCSTCDQ is "$nsigTCSTCDQ" sigma"   
echo "TDI is "$nsigTDI" sigma"   
echo "TCTH1 is "$nsigTCTH1" sigma"   
echo "TCTH2 is "$nsigTCTH2" sigma"   
echo "TCTH5 is "$nsigTCTH5" sigma"   
echo "TCTH8 is "$nsigTCTH8" sigma"   
echo "TCTV1 is "$nsigTCTV1" sigma"   
echo "TCTV2 is "$nsigTCTV2" sigma"   
echo "TCTV5 is "$nsigTCTV5" sigma"   
echo "TCTV8 is "$nsigTCTV8" sigma"   
echo "TCXRP is "$nsigTCXRP" sigma"   
echo "TCRYO is "$nsigTCRYO" sigma" 
sed -i 's/NSIG_TCP3/'"$nsigTCP3"'/g' fort.3
sed -i 's/NSIG_TCSG3/'"$nsigTCSG3"'/g' fort.3   
sed -i 's/NSIG_TCSM3/'"$nsigTCSM3"'/g' fort.3   
sed -i 's/NSIG_TCLA3/'"$nsigTCLA3"'/g' fort.3     
sed -i 's/NSIG_TCP7/'"$nsigTCP7"'/g' fort.3   
sed -i 's/NSIG_TCSG7/'"$nsigTCSG7"'/g' fort.3      
sed -i 's/NSIG_TCSM7/'"$nsigTCSM7"'/g' fort.3      
sed -i 's/NSIG_TCLA7/'"$nsigTCLA7"'/g' fort.3       
sed -i 's/NSIG_TCLP/'"$nsigTCLP"'/g' fort.3        
sed -i 's/NSIG_TCLI/'"$nsigTCLI"'/g' fort.3       
sed -i 's/NSIG_TCDQ/'"$nsigTCDQ"'/g' fort.3 
sed -i 's/NSIG_TCSTCDQ/'"$nsigTCSTCDQ"'/g' fort.3   
sed -i 's/NSIG_TDI/'"$nsigTDI"'/g' fort.3   
sed -i 's/NSIG_TCTH1/'"$nsigTCTH1"'/g' fort.3   
sed -i 's/NSIG_TCTH2/'"$nsigTCTH2"'/g' fort.3   
sed -i 's/NSIG_TCTH5/'"$nsigTCTH5"'/g' fort.3   
sed -i 's/NSIG_TCTH8/'"$nsigTCTH8"'/g' fort.3   
sed -i 's/NSIG_TCTV1/'"$nsigTCTV1"'/g' fort.3   
sed -i 's/NSIG_TCTV2/'"$nsigTCTV2"'/g' fort.3   
sed -i 's/NSIG_TCTV5/'"$nsigTCTV5"'/g' fort.3   
sed -i 's/NSIG_TCTV8/'"$nsigTCTV8"'/g' fort.3   
sed -i 's/NSIG_TCXRP/'"$nsigTCXRP"'/g' fort.3   
sed -i 's/NSIG_TCRYO/'"$nsigTCRYO"'/g' fort.3     

# setting remaining parameters
sed -i 's/NTURNS/10/g' fort.3
sed -i 's/NPART/1/g' fort.3
sed -i 's/DISTSIGX/'"$nsigTCP7"'/g' fort.3
sed -i 's/DISTXSPREAD/0.001/g' fort.3
sed -i 's/DISTSIGY/0/g' fort.3
sed -i 's/DISTYSPREAD/0/g' fort.3
sed -i 's/EXN/'"$6"'/g' fort.3
sed -i 's/EYN/'"$6"'/g' fort.3
sed -i 's/NRJ/'"$7"'/g' fort.3
if [ $3 = H ]
then
    echo "H"
    ipencil=30
    if [ $2 = b1 ]
    then
        firstColl=TCP.C6L7.B1
    else
        firstColl=TCP.C6R7.B2
    fi
else
    echo "V"
    ipencil=29
    if [ $2 = b1 ]
    then
        firstColl=TCP.D6L7.B1
    else
        firstColl=TCP.D6R7.B2
    fi
fi
echo "ipencil is "$ipencil
sed -i 's/IPENCIL/'"$ipencil"'/g' fort.3

# calculate impact parameter in sigma
# this code is left in but deprecated with the addition of the pyCollSoft gpconfig generator by Andrey
if [ $3 = H ]
then
    cat LHC_$2_7000GeV_thinXing.twiss | grep NAME | tail --lines=1 > remove.txt
    if [ ! -s remove.txt ]
    then
        echo "Twiss header not found, setting default impact parameter to 0.001 of TCP setting."
        IMPACTPARAM=$(awk 'BEGIN {printf '"$nsigTCP7"'/1000}')
        echo "Impact parameter is "$IMPACTPARAM" sigma"
    else
        BETXCOL=$(awk -v name='BETX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
        BETXCOL=$((BETXCOL-1)) 
        cat LHC_$2_7000GeV_thinXing.twiss | grep TCP.C6 | head --lines=1 > remove2.txt
        BETX=$(awk '{print $'"$BETXCOL"'}' remove2.txt)
        echo "BETX is "$BETX" m"
        IMPACTPARAM=$(awk 'BEGIN {printf 2*'"${10}"'/1000/sqrt('"$BETX"'*'"$6"'*'"$PMASS"'/'"$7"')/11.74}')
    fi
else
    cat LHC_$2_7000GeV_thinXing.twiss | grep NAME | tail --lines=1 > remove.txt
    if [ ! -s remove.txt ]
    then
        echo "Twiss header not found, setting default impact parameter to 0.001 of TCP setting."
        IMPACTPARAM=$(awk 'BEGIN {printf '"$nsigTCP7"'/1000}')
        echo "Impact parameter is "$IMPACTPARAM" sigma"
    else
        BETYCOL=$(awk -v name='BETX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
        BETYCOL=$((BETYCOL-1)) 
        cat LHC_$2_7000GeV_thinXing.twiss | grep TCP.D6 | head --lines=1 > remove2.txt
        BETY=$(awk '{print $'"$BETYCOL"'}' remove2.txt)
        echo "BETY is "$BETY" m"
        IMPACTPARAM=$(awk 'BEGIN {printf 2*'"${10}"'/1000/sqrt('"$BETY"'*'"$6"'*'"$PMASS"'/'"$7"')/5.46}')
    fi  
fi
echo "Impact parameter is "$IMPACTPARAM" sigma" 


# run sixtrack to get collgaps file
sixtrackDefault > sixtrack.out
tail sixtrack.out

# create raw_input folder and copy necessary files there
mkdir ../raw_input
cp fort3.limi CollDB_HL.data collgaps.dat CollPositions.dat collsettings.dat fort.2 fort.3 fort.8 LHC_$2_7000GeV_thinXing.twiss ../raw_input/ 

# create folder for preprocessed input files and move there
mkdir ../preprocessed_input
cd ../preprocessed_input 
ln -s ../raw_input/* .
ln -s /afs/cern.ch/work/b/bjlindst/private/fluka_coupling/tools/preprocess .
ln -s /afs/cern.ch/work/b/bjlindst/private/fedb_tools .

if [ -z "${16}" ]
then
    echo "Arg 16 not found - using default prototypes.lbp file."
    cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/HLLHCV1.5_$2_prototypes.lbp ./prototypes.lbp
else
    echo "Arg 16 found - using specified prototypes.lbp file."
    cp ${16} ./prototypes.lbp
fi  

# ln -s ../sixtrackK2run_1/offset_B1.dat ./ # only required for generate_sixtrack_apertures.py

#python2.7 preprocess/FLUKA_builder.py --CollG collgaps.dat --EMIn $6E-06 --pc $7 --TFile LHC_$2_7000GeV_thinXing.twiss
python3 /eos/project-f/flukafiles/fluka-coupling/py3/fluka_coupling/tools/preprocess/FLUKA_builder.py --CollG collgaps.dat --EMIn $6E-06 --pc $7 --TFile LHC_$2_7000GeV_thinXing.twiss --FEDB /afs/cern.ch/work/b/bjlindst/private/fedb_coupling

mv lhc_coupling.insertion.txt insertion.txt 
cp /eos/project-f/flukafiles/fluka-coupling/fluka_coupling/tools/template_inputs/include_templates/include_* ./
##############
# Change include_settings_beam.inp and include_settings_physics.inp here if necessary
##############


./fedb_tools/expand.sh lhc_coupling.inp 
rm relcol.dat
if [ ${11} = all ]
then
    echo "all collimators in relcol"
    wc -l lhc_coupling.fort3.list | awk '{printf $1"\n"}' >> relcol.dat
    awk '{printf $3" "}' lhc_coupling.fort3.list >> relcol.dat  
elif [ ${11} = tct ]
then
    echo "TCT and experiment TCLs in relcol"
    < lhc_coupling.fort3.list grep "tc[tl].*[1258].b" | wc -l >> relcol.dat
    < lhc_coupling.fort3.list grep "tc[tl].*[1258].b" | awk '{printf $3" "}' >> relcol.dat
elif [ ${11} = tcs ]
then
    echo "TCS in relcol"
    < lhc_coupling.fort3.list grep "tcs.*" | wc -l >> relcol.dat
    < lhc_coupling.fort3.list grep "tcs.*" | awk '{printf $3" "}' >> relcol.dat
elif [ ${11} = ir7 ]
then
    echo "all IR7 collimators in relcol"
    < lhc_coupling.fort3.list grep ".*[7].b" | wc -l >> relcol.dat
    < lhc_coupling.fort3.list grep ".*[7].b" | awk '{printf $3" "}' >> relcol.dat  
elif [ ${11} = ir3 ]
then
    echo "all IR3 collimators in relcol"
    < lhc_coupling.fort3.list grep ".*[3].b" | wc -l >> relcol.dat
    < lhc_coupling.fort3.list grep ".*[3].b" | awk '{printf $3" "}' >> relcol.dat
else
    echo "no/incorrect relcol selection input [arg11]. Defaulting to TCPs only."
    < lhc_coupling.fort3.list grep "tcp" | wc -l >> relcol.dat
    < lhc_coupling.fort3.list grep "tcp" | awk '{printf $3" "}' >> relcol.dat 
fi

#python2.7 preprocess/fort2_markers.py fort.2 lhc_coupling.fort3.list # add FLUKA-collimator markers in fort.2 -> fort_w_markers.2
python3 /eos/project-f/flukafiles/fluka-coupling/py3/fluka_coupling/tools/preprocess/fort2_markers.py fort.2 lhc_coupling.fort3.list
# python2.7 preprocess/generate_sixtrack_apertures.py fort_w_markers.2 LHC_b1_7000GeV_thinXing.twiss offset_B1.dat # creates fort3.limi and new_fort.2, not required since fort3.limi from MADX is available and better
if [ -z "${15}" ]
then
    echo "Arg 15 not found - using fort.3 file without aperture offsets"
    cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/fort.3_FLUKA ./fort.3 
else
    echo "Arg 15 found - using fort.3 file with aperture offsets"
    cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/fort.3_FLUKA_OFS ./fort.3
fi
sixtrackFLUKA_insertCollsInFort3.sh lhc_coupling.fort3.list fort.3
sed -i 's/NTURN/'"${12}"'/g' fort.3
sed -i 's/NPART/'"${13}"'/g' fort.3
sed -i 's/EXN/'"$6"'/g' fort.3
sed -i 's/EYN/'"$6"'/g' fort.3
sed -i 's/NRJ/'"$7"'/g' fort.3

# get particle distribution config file and input starting parameters, for now, starting in IP1
# this code is left in but is deprecated with the addition of the pyCollSoft gpconfig generator by Andrey
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/config_gpdist.txt ./
adjustConfigGPDIST.sh LHC_$2_7000GeV_thinXing.twiss $4 $3 $nsigTCP7 $(awk 'BEGIN {printf '"$nsigTCP7"'+'"$IMPACTPARAM"'}') $6



# create the clean_input folder

mkdir ../clean_input$5$3 # $5 is unique identifier, $3 is H / V

#cp new_fort.2 fort.3 lhc_coupling_exp.inp insertion.txt config_gpdist.txt fort3.limi relcol.dat ../clean_input$5$3
cp collgaps.dat fort.3 fort_w_markers.2 lhc_coupling_exp.inp insertion.txt config_gpdist.txt fort3.limi relcol.dat fort.8 ../clean_input$5$3
cd ../clean_input$5$3/
mv fort_w_markers.2 fort.2 
if [ ! $4 = IP1 ]
then
    startPos=$(echo "$4" | awk '{print tolower($0)}')
    echo "Inserting GO statement in fort.2 at "$startPos
    sed -i ':a;N;$!ba; s/'"$startPos"[^_]'/GO '"$startPos"'/2' fort.2  
else
    echo "Starting at IP1"
fi      
#mv new_fort.2 fort.2    
#sed -i '2iatlaspipe1.r1_AP EL  2.3500e+01 2.3500e+01 0.0000e+00 0.0000e+00 -0.0000e+00 -0.0000e+00 0.0000e+00' fort3.limi
### comment - for b2, should add atlaspipe1.l1_AP ###

python /afs/cern.ch/work/b/bjlindst/public/python/python_packages/pyflusix/bin/make_beam_collimator.py --fort2=fort.2 --fort3=fort.3 --collGaps=collgaps.dat --tfsOptics=../preprocessed_input/LHC_$2_7000GeV_thinXing.twiss --collName=$firstColl --enorm=$6E-06 --blen=2.5E-09 --deltap=1.129E-04 --imp=${10}E-06
rm config_gpdist.txt
mv CHECKME_gpdist_config.txt config_gpdist.txt

# go to root dir and add the submission scripts
cd ..
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/control.sh ./
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/htcondor.sub ./
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/lhc_coupling_exp.job ./   

# run control script
./control.sh -p -d runSingle$5$3 -i clean_input$5$3
cd runSingle$5$3
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/sixtrackFLUKA_initial.dat ./initial.dat
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/sixtrackFLUKA_execute.sh ./execute.sh
cp clean_input/* ./ 
