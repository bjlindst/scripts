#!/bin/bash

echo "Concatenating lossMap.dat"
gzip -d */lhc_coupling_exp001_lossMap.dat.gz
awk '$8<4 {printf $1"\n"}' */lhc_coupling_exp001_lossMap.dat > collSummaryTemp1.dat
awk '$8>1 {printf $1"\n"}' */lhc_coupling_exp001_lossMap.dat > collSummaryTemp4.dat
gzip */lhc_coupling_exp001_lossMap.dat

echo "Concatenating aperture_losses.dat"
gzip -d */aperture_losses.dat.gz
awk '$1>0 {printf $5"\n"}' */aperture_losses.dat > aperLossesTemp.dat
gzip */aperture_losses.dat
