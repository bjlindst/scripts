#!/bin/bash

Help()
{
    echo "This script will output the requested twiss parameters for elements with a name decided by grep."
    echo 
    echo "Run the script as getTwissParams.sh arg1 arg2 arg3 arg4 ...,"
    echo "where arg1 is the path to a twiss file, arg2 is 1 for RegEx matching or 0 for normal grep,"
    echo "arg3 is the match to use for grep. arg4 and onwards are an optional number of arguments"
    echo "indicate which twiss params are to be output."
}

while getopts ":h" option; do
    case $option in 
        h) # display Help
        Help
        exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 is not provided. Run getTwissParams.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 is not provided. Run getTwissParams.sh -h for help."
    exit;
fi
if [ -z "$3" ]
then
    echo "argument 3 is not provided. Run getTwissParams.sh -h for help."
    exit;
fi
if [ -z "$4" ]
then
    echo "argument 4 is not provided. A minimum of 4 arguments are necessary,"
    echo "with at least one twiss parameter to be chosen."
    exit;
fi

startRow=$(grep 'NAME' $1 | tail --lines=1)
if [ -z "$startRow" ]
then
    echo "Twiss header not found, please check the file name. Aborting..."
    exit 1
fi
#echo "$startRow"
awkStr='{printf $1"\t'
header='NAME'
for var in "$@"
do
    if [ $var != $1 ] && [ $var != $2 ] && [ $var != $3 ]
    then
        #echo "$var"
        COL=$(awk -v name=$var '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' <<< "$startRow")
        if [ -z $COL ]
        then
            echo $var" not found in twiss file. Please check spelling and twiss header. Aborting..."
            exit 1
        fi
        COL=$((COL-1))
        #echo $COL
        awkStr=$awkStr'"$'$COL'"\t'
        header=$header'\t\t'$var
    fi
done
awkStr=$awkStr'\n"}'
#echo "$awkStr"

if [ $2 == 0 ]
then
    echo "no regex"
    echo -e $header
    grep $3 $1 | awk "$awkStr"
else
    echo "regex"
    echo -e $header
    grep -E $3 $1 | awk "$awkStr"
fi 




