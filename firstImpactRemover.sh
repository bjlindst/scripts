ls */FirstImpacts.dat > firstImpactFiles.temp
firstFile=$(head firstImpactFiles.temp --lines=1)
echo "Copying "$firstFile" to root dir and removing the rest."
cp $firstFile ./
rm */FirstImpacts.dat
