ls */lhc_coupling_exp001_first.dat.gz > firstImpactFiles.temp
firstFile=$(head firstImpactFiles.temp --lines=1)
echo "Copying "$firstFile" to root dir and removing the rest."
cp $firstFile ./
rm */lhc_coupling_exp001_first.dat.gz
rm firstImpactFiles.temp

ls */tracker*.log.gz > trackerFiles.temp
trackerFile=$(head trackerFiles.temp --lines=1)
echo "Copying "$trackerFile" to root dir and removing the rest."
cp $trackerFile ./
rm */tracker*.log.gz
rm trackerFiles.temp
