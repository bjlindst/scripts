#!/bin/bash
Help()
{
    echo "This script copies a config_gpdist.txt template file and"
    echo " inputs the correct initial conditions depending on where"
    echo " you want the particles to start."
    echo
    echo "Run the script as adjustConfigGPDIST.sh arg1 arg2 arg3 arg4"
    echo "where arg1 is the twiss file, arg2 is the name of the element, arg3 the lossplane H or V, "
    echo "arg4 and arg5 the min and max sigma of the annulus, arg7 the normalized x/y emittance [µm]."
    echo
    echo "NOTE: the element name is case sensitive and must be written exactly as in the twiss file, within double quotation \" ."
}    

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done



if [ -z "$1" ]
then
    echo "argument 1 not provided. Run adjustConfigGPDIST.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run adjustConfigGPDIST.sh -h for help."
    exit;
fi 
if [ -z "$3" ]
then
    echo "argument 3 not provided. Run adjustConfigGPDIST.sh -h for help."
    exit;
fi   
if [ -z "$4" ]
then
    echo "argument 4 not provided. Run adjustConfigGPDIST.sh -h for help."
    exit;
fi 
if [ -z "$5" ]
then
    echo "argument 5 not provided. Run adjustConfigGPDIST.sh -h for help."
    exit;
fi     
if [ -z "$6" ]
then
    echo "argument 6 not provided. Run adjustConfigGPDIST.sh -h for help."
    exit;
fi                          


cat $1 | grep NAME | tail --lines=1 > remove.txt

if [ ! -s remove.txt ]
then
    echo "Twiss header not found, exiting..."
    exit;
fi

XCOL=$(awk -v name='X' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
XCOL=$((XCOL-1))
YCOL=$(awk -v name='Y' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
YCOL=$((YCOL-1))
PXCOL=$(awk -v name='PX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
PXCOL=$((PXCOL-1))
PYCOL=$(awk -v name='PY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
PYCOL=$((PYCOL-1))
BETXCOL=$(awk -v name='BETX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
BETXCOL=$((BETXCOL-1))
BETYCOL=$(awk -v name='BETY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
BETYCOL=$((BETYCOL-1))
ALFXCOL=$(awk -v name='ALFX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
ALFXCOL=$((ALFXCOL-1))
ALFYCOL=$(awk -v name='ALFY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
ALFYCOL=$((ALFYCOL-1))
DXCOL=$(awk -v name='DX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
DXCOL=$((DXCOL-1))
DYCOL=$(awk -v name='DY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
DYCOL=$((DYCOL-1))
DPXCOL=$(awk -v name='DPX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
DPXCOL=$((DPXCOL-1))
DPYCOL=$(awk -v name='DPY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
DPYCOL=$((DPYCOL-1))
NAMECOL=$(awk -v name='NAME' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
NAMECOL=$((NAMECOL-1))
         
cat $1 | grep "\<$2\>" | head --lines=1 > remove2.txt 
if [ ! -s remove2.txt ]
then
    echo "Element not found, check spelling. Exiting..."
    exit;
fi    


X=$(awk '{print $'"$XCOL"'}' remove2.txt)
Y=$(awk '{print $'"$YCOL"'}' remove2.txt)  
PX=$(awk '{print $'"$PXCOL"'}' remove2.txt)  
PY=$(awk '{print $'"$PYCOL"'}' remove2.txt)  
BETX=$(awk '{print $'"$BETXCOL"'}' remove2.txt)  
BETY=$(awk '{print $'"$BETYCOL"'}' remove2.txt)  
ALFX=$(awk '{print $'"$ALFXCOL"'}' remove2.txt)  
ALFY=$(awk '{print $'"$ALFYCOL"'}' remove2.txt)  
DX=$(awk '{print $'"$DXCOL"'}' remove2.txt)  
DY=$(awk '{print $'"$DYCOL"'}' remove2.txt)  
DPX=$(awk '{print $'"$DPXCOL"'}' remove2.txt)  
DPY=$(awk '{print $'"$DPYCOL"'}' remove2.txt)  
NAME=$(awk '{print $'"$NAMECOL"'}' remove2.txt) 

echo "Copying config_gpdist.txt from /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates..."
cp /afs/cern.ch/work/b/bjlindst/public/sixtrack/inputFilesFLUKA/templates/config_gpdist.txt ./
if [ ! -s config_gpdist.txt ]
then
    echo "Could not copy config_gpdist.txt template. Exiting..."
    exit;
fi   

echo "Inserting the following into config_gpdist.txt:"
echo "XCOL = $XCOL --- X = $X"  
echo "YCOL = $YCOL --- Y = $Y"  
echo "PXCOL = $PXCOL --- PX = $PX"  
echo "PYCOL = $PYCOL --- PY = $PY"   
echo "BETXCOL = $BETXCOL --- BETX = $BETX"
echo "BETYCOL = $BETYCOL --- BETY = $BETY"    
echo "ALFXCOL = $ALFXCOL --- ALFX = $ALFX"   
echo "ALFYCOL = $ALFYCOL --- ALFY = $ALFY"    
echo "DXCOL = $DXCOL --- DX = $DX"    
echo "DYCOL = $DYCOL --- DY = $DY" 
echo "DPXCOL = $DPXCOL --- DPX = $DPX"   
echo "DPYCOL = $DPYCOL --- DPY = $DPY"    
echo "NAMECOL = $NAMECOL --- NAME = $NAME"     

sed -i 's/twissX/'"$X"'/g' config_gpdist.txt
sed -i 's/twissY/'"$Y"'/g' config_gpdist.txt 
sed -i 's/twissPX/'"$PX"'/g' config_gpdist.txt 
sed -i 's/twissPY/'"$PY"'/g' config_gpdist.txt 
sed -i 's/twissBETX/'"$BETX"'/g' config_gpdist.txt 
sed -i 's/twissBETY/'"$BETY"'/g' config_gpdist.txt 
sed -i 's/twissALFX/'"$ALFX"'/g' config_gpdist.txt 
sed -i 's/twissALFY/'"$ALFY"'/g' config_gpdist.txt 
sed -i 's/twissDX/'"$DX"'/g' config_gpdist.txt 
sed -i 's/twissDY/'"$DY"'/g' config_gpdist.txt 
sed -i 's/twissDPX/'"$DPX"'/g' config_gpdist.txt 
sed -i 's/twissDPY/'"$DPY"'/g' config_gpdist.txt
sed -i 's/twissNAME/'"$NAME"'/g' config_gpdist.txt 

if [ $3 = H ]
then
    echo "Loss plane set to horizontal."
    sed -i 's/LOSSPLANE/1/g' config_gpdist.txt
    sed -i 's/OFFPLANE/2/g' config_gpdist.txt
else
    echo "Loss plane set to vertical."
    sed -i 's/LOSSPLANE/2/g' config_gpdist.txt
    sed -i 's/OFFPLANE/1/g' config_gpdist.txt
fi
sed -i 's/SIGMIN/'"$4"'/g' config_gpdist.txt
sed -i 's/SIGMAX/'"$5"'/g' config_gpdist.txt
sed -i 's/EXN/'"$6"'/g' config_gpdist.txt
sed -i 's/EYN/'"$6"'/g' config_gpdist.txt

rm remove.txt remove2.txt          
