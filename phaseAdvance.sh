#!/bin/bash

Help()
{
    echo "This script outputs the horizontal and vertical phase advance between two elements."
    echo
    echo "Run the script as phaseAdvance.sh arg1 arg2 arg3 arg4 arg5,"
    echo "where arg1 is the path to a twiss file, arg2 the first element, arg3 the second element."
    echo "arg4 set to 1 activates debug printout and arg5 set to 1 flips the s order (b2 files)."
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run phaseAdvance.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run phaseAdvance.sh -h for help."
    exit;
fi   
if [ -z "$3" ]
then
    echo "argument 3 not provided. Run phaseAdvance.sh -h for help."
    exit;
fi   

startRow=$(grep 'NAME' $1 | tail --lines=1)
if [ -z "$startRow" ]
then
    echo "Twiss header not found, please check the file name. Aborting..."
    exit 1
else
    if [[ "$4" -eq 1 ]]; then 
        echo "$startRow"
    fi
fi

MUXCOL=$(awk -v name='MUX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' <<< "$startRow")
if [ -z "$MUXCOL" ]
then
    echo "MUX column not found. Please check your twiss file. Aborting..."
    exit 1
fi
MUXCOL=$((MUXCOL-1))
MUYCOL=$(awk -v name='MUY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' <<< "$startRow")
if [ -z $MUYCOL ]
then
    echo "MUY column not found. Please check your twiss file. Aborting..."
    exit 1
fi 
MUYCOL=$((MUYCOL-1))
firstElem=$(grep -w $2 $1)
if [ -z "$firstElem" ]
then
    echo "First element ("$2") not found. Aborting..."
    exit 1
fi
seconElem=$(grep -w $3 $1)
if [ -z "$seconElem" ]
then
    echo "Second element ("$3") not found. Aborting..."
    exit 1
fi 
lastElem=$(tail --lines=1 $1)  
MUX1=$(awk '{printf $'"$MUXCOL"'}' <<< "$firstElem")
MUY1=$(awk '{printf $'"$MUYCOL"'}' <<< "$firstElem")
MUX2=$(awk '{printf $'"$MUXCOL"'}' <<< "$seconElem")
MUY2=$(awk '{printf $'"$MUYCOL"'}' <<< "$seconElem")
MUX3=$(awk '{printf $'"$MUXCOL"'}' <<< "$lastElem")
MUY3=$(awk '{printf $'"$MUYCOL"'}' <<< "$lastElem")
if [[ "$4" -eq 1 ]];
then
    echo "$firstElem"
    echo "$seconElem"
    echo "First element MUX: "$MUX1
    echo "First element MUY: "$MUY1
    echo "Second element MUY: "$MUX2
    echo "Second element MUY: "$MUY2
    echo "Qx: "$MUX3
    echo "Qy: "$MUY3
fi

tempMUX2=$(echo $MUX2 | awk '{printf 1.0*$1}')
tempMUX1=$(echo $MUX1 | awk '{printf 1.0*$1}')

if (( $(echo "$tempMUX2 < $tempMUX1" | bc -l) ))
then
    if [[ "$5" -eq 1 ]];
    then
        tempMessage="Do not add the tune, reverse order"
        deltaMUX=$(awk 'BEGIN {printf '"$MUX1"'-'"$MUX2"'}')
        deltaMUY=$(awk 'BEGIN {printf '"$MUY1"'-'"$MUY2"'}')
    else
        tempMessage="Add the tune"
        deltaMUX=$(awk 'BEGIN {printf '"$MUX3"'-'"$MUX1"'+'"$MUX2"'}')
        deltaMUY=$(awk 'BEGIN {printf '"$MUY3"'-'"$MUY1"'+'"$MUY2"'}')
    fi
else
    if [[ "$5" -eq 1 ]];
    then
        tempMessage="Add the tune, reverse order"
        deltaMUX=$(awk 'BEGIN {printf '"$MUX3"'-'"$MUX2"'+'"$MUX1"'}')
        deltaMUY=$(awk 'BEGIN {printf '"$MUY3"'-'"$MUY2"'+'"$MUY1"'}')
    else
        tempMessage="Do not add the tune"
        deltaMUX=$(awk 'BEGIN {printf '"$MUX2"'-'"$MUX1"'}')
        deltaMUY=$(awk 'BEGIN {printf '"$MUY2"'-'"$MUY1"'}')
    fi
fi

fracMUX=$(awk 'BEGIN {printf '"$deltaMUX"' % 1}')
fracMUY=$(awk 'BEGIN {printf '"$deltaMUY"' % 1}') 
phaseAdvX=$(awk 'BEGIN {printf '"$fracMUX"'*360}')
phaseAdvY=$(awk 'BEGIN {printf '"$fracMUY"'*360}')
if (( $( echo "$phaseAdvX > 180" | bc -l) ))
then
    tempMessage2="phaseAdvX - "$phaseAdvX" - is larger than 180."
    phaseAdvX=$(awk 'BEGIN {printf 360-'"$phaseAdvX"'-180}')
fi
if (( $( echo "$phaseAdvY > 180" | bc -l) ))
then
    tempMessage3="phaseAdvY - "$phaseAdvY" - is larger than 180."
    phaseAdvY=$(awk 'BEGIN {printf 360-'"$phaseAdvY"'-180}')
fi
if [[ "$4" -eq 1 ]];
then
    echo $tempMessage
    echo "Delta MUX: "$deltaMUX
    echo "Delta MUY: "$deltaMUY
    echo "Fractional part of MUX: "$fracMUX
    echo "Fractional part of MUY: "$fracMUY
    echo $tempMessage2
    echo $tempMessage3
fi   
echo "----------------------------------------------------"
echo "-----------------------RESULT-----------------------"
echo "----------------------------------------------------"
echo
echo "Horizontal phase advance [deg]: "$phaseAdvX
echo "Vertical phase advance [deg]: "$phaseAdvY
 


