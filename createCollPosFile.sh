#!/bin/bash

Help()
{
    echo "This script creates a CollPositions.bx.dat file as normally created by SixTrack."
    echo
    echo "Run the script as createCollPosFile.sh arg1 arg2 arg3,"
    echo "where arg1 is bx (only used for the name of the output file), arg2 the path to a collgaps file, arg3 the path to a twiss file."
    echo "For b2, please use a b4 twiss file to get the correct positions as assumed by the postprocessing tools."

}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run createCollPosFile.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run createCollPosFile.sh -h for help."
    exit;
fi   
if [ -z "$3" ]
then
    echo "argument 3 not provided. Run createCollPosFile.sh -h for help."
    exit;
fi   

echo -e "%Ind\tName\tPos[m]" > CollPositions.$1.dat

numColls=$(wc -l $2 | awk '{printf $1}')

echo $numColls
echo $1

for line in $(seq 2 $numColls)
do
    echo $((line-1))
    coll=$(sed -n "$line p" $2 | awk '{printf $2}')
    SCOL=$(< $3 grep NAME | tail --lines=1 | awk -v name='S' '{for (i=1;i<=NF;i++) if ($i==name) print i-1; exit}')
    echo $coll
    #colline=$(< $3 grep -i -w "$coll")
    collpos=$(< $3 grep -i -w [^SE]"$coll" | awk '{print $'"$SCOL"'}')
    #echo $colline
    echo $collpos
    echo -e $((line-1))"\t"$coll"\t"$collpos >> CollPositions.$1.dat
done






