#!/bin/bash

#echo "Concatenating lossMap.dat"
#gzip -d */lhc_coupling_exp001_lossMap.dat.gz
#awk '$8<4 {printf $1"\n"}' */lhc_coupling_exp001_lossMap.dat > collSummaryTemp1.dat
#awk '$8>1 {printf $1"\n"}' */lhc_coupling_exp001_lossMap.dat > collSummaryTemp4.dat
#gzip */lhc_coupling_exp001_lossMap.dat

#echo "Concatenating aperture_losses.dat"
#gzip -d */aperture_losses.dat.gz
#awk '$1>0 {printf $5"\n"}' */aperture_losses.dat > aperLossesTemp.dat
#gzip */aperture_losses.dat


ls | grep run_ > runList.txt
input=runList.txt
while IFS= read -r line
do
    #echo $line
    if [ -f $line"/lhc_coupling_exp001_lossMap.dat.gz" ]; then
        echo $line
        echo "coll losses found"
        gzip -d $line"/lhc_coupling_exp001_lossMap.dat.gz"
        awk '$8<4 {printf $1"\n"}' $line/lhc_coupling_exp001_lossMap.dat > $line/collSummaryTemp1.dat
        awk '$8>1 {printf $1"\n"}' $line/lhc_coupling_exp001_lossMap.dat > $line/collSummaryTemp4.dat
        gzip $line/lhc_coupling_exp001_lossMap.dat  
        if [ -f $line"/collSummary1tallied.dat" ]; then
            rm $line/collSummary1tallied.dat
        fi
        if [ -f $line"/collSummary4tallied.dat" ]; then
            rm $line/collSummary4tallied.dat
        fi
        for coll in {1..57}
        do
            val=$(grep -w $coll $line/collSummaryTemp1.dat | wc -l)
            echo $coll" "$val >> $line/collSummary1tallied.dat
            val=$(grep -w $coll $line/collSummaryTemp4.dat | wc -l)
            echo $coll" "$val >> $line/collSummary4tallied.dat
        done
        rm $line/collSummaryTemp1.dat
        rm $line/collSummaryTemp4.dat
    fi
    if [ -f $line"/aperture_losses.dat.gz" ]; then
        echo $line
        echo "aperture_losses found"
        gzip -d $line/aperture_losses.dat.gz
        awk '$1>0 {printf $5"\n"}' $line/aperture_losses.dat > $line/aperLossesTemp.dat
        gzip $line/aperture_losses.dat
        cat $line/aperLossesTemp.dat | sort | uniq -c | awk '{printf $2" "$1"\n"}' > $line/aperLossesTallied.dat
        rm $line/aperLossesTemp.dat
    fi
done < "$input"
rm runList.txt
