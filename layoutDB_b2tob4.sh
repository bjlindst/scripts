#!/bin/bash

Help()
{
    echo "This script takes a layoutDB MADX sequence file, containing definitions for both b1 and b2, and"
    echo "splits it into two parts, one with b1 and one with b2, and then flips the sign of the install pos"
    echo "for b2, thus creating the definitions for b4."
    echo ""
    echo "arg1 is the layoutDB file you want to split."
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run layoutDB_b2tob4.sh -h for help."
    exit;
fi   


splitLine=$(cat $1 | grep -n endedit | head --lines=1 | cut -d : -f 1)
echo $splitLine
split -l $splitLine $1 temp
fileName=${1%.*}"_b4_part"
echo $fileName

sed -i 's/at= /at= -/g' tempab
sed -i 's/--//g' tempab
sed -i 's/-\./-0./g' tempab
sed -i 's/+(/-(/g' tempab # this corrects the DS-offset due to the dipole magnets, necessary for consistent apertures b1/b4

sed -i 's/mech_sep= / mech_sep= -/g' tempaa
sed -i 's/-0,/0,/g' tempaa
sed -i 's/--//g' tempaa

mv tempaa $fileName"1.seq"
mv tempab $fileName"2.seq"

