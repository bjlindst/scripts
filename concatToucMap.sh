#! /bin/bash

gzip -d */lhc_coupling_exp001_toucMap.dat.gz
head run_00001/lhc_coupling_exp001_toucMap.dat --lines=1
head run_00001/lhc_coupling_exp001_toucMap.dat --lines=1 > toucMap_concat.dat
tail -q --lines=+2 */lhc_coupling_exp001_toucMap.dat >> toucMap_concat.dat
gzip */lhc_coupling_exp001_toucMap.dat
