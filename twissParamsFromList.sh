#!/bin/bash

Help()
{
    echo "This script outputs a certain twiss parameter for a list of elements."
    echo
    echo "Run the script as twissParamsFromList.sh arg1 arg2 arg3 arg4,"
    echo "where arg1 is the path to a twiss file, arg2 is the path to a list of elements."
    echo "arg3 is the twiss parameter of interest (case sensitive) and arg4 adds debug printout."
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run phaseAdvance.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run phaseAdvance.sh -h for help."
    exit;
fi
if [ -z "$3" ]
then
    echo "argument 3 not provided. Run phaseAdvance.sh -h for help."
    exit;
fi      

startRow=$(grep 'NAME' $1 | tail --lines=1)
if [ -z "$startRow" ]
then
    echo "Twiss header not found, please check the file name. Aborting..."
    exit 1
else
    if [[ $4 ]]; then 
        echo "$startRow"
    fi
fi

TWISSCOL=$(awk -v name=$3 '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' <<< "$startRow")
if [ -z "$TWISSCOL" ]
then
    echo $3" column not found. Note that it is case sensitive. Printing out twiss header and aborting..."
    echo "$startRow"
    exit 1
fi
TWISSCOL=$((TWISSCOL-1))

input=$2
while IFS= read -r line
do
    element=$(grep $line $1)
    if [ -z "$element" ]
    then
        echo "WARNING: element "$line" not found."
    fi
    value=$(awk '{printf $'"$TWISSCOL"'}' <<< "$element")
    echo -e $line"\t"$value
    if [[ $4 ]];
    then
        echo $element
    fi
done < "$input" 
