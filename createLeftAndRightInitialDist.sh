# takes a standard dump file from SixTrack, named dump_hejsan, extracts all the particles that are originating from the left TCP jaw (negative x), removes unneccessary columns. It then mirrors this around horizontal axis to create a corresponding initial distribution for particles coming from the right jaw.
#
# The columns in the resulting distribution files are: 
#                    
# 1 turn                      - removed
# 2 structure_element_idx     - removed
# 3 single_element_idx        - removed
# 4 single_element_name       - removed
# 5 s                         - removed
# 6 x[m]                      - kept
# 7 xp[rad]                   - kept
# 8 y[m]                      - kept
# 9 yp[rad]                   - kept
# A P[GeV/c]                  - kept
# B (E-E0)[eV]                - kept
# C t[s]                      - removed
#


awk '$6<0 {print $0}' dump_hejsan > hejsan_sorted.dat
awk '{printf $6" "$7" "$8" "$9" "$10" "$11"\n"}' hejsan_sorted.dat > hejsan_sorted2.dat
cp hejsan_sorted2.dat ../pencil_initialDist_left.dat
sed 's/./&-/26' pencil_initialDist_left.dat > pencil_initialDist_right.dat
sed -i 's/--//g' pencil_initialDist_right.dat
sed -i 's/^.\{1\}//g' pencil_initialDist_right.dat



