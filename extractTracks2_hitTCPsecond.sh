#! /bin/bash

awk '$1 == 30 && $10 == 2 {print $0}' FLUKA_impacts_all.dat > temp_hits30.dat
awk '{print $9}' temp_hits30.dat > temp_hits30_np.dat 

input=temp_hits30_np.dat
while IFS= read -r line
do
    echo "$line"
    awk '$1 == '"$line"' {print $0}' tracks2.dat > tracksExtracted"$line".dat
done < "$input"




