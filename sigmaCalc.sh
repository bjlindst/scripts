#!/bin/bash
Help()
{
    echo "This script calculates the beam RMS betatron sigma."
    echo "Usage: sigmaCalc.sh beta energy emittance"
    echo "where beta is the beta function in meter, energy in GeV and "
    echo "emittance being the normalized emittance in mm.mrad."
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run sigmaCalc.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run sigmaCalc.sh -h for help."
    exit;
fi

if [ -z "$3" ]
then
    echo "argument 3 not provided. Run sigmaCalc.sh -h for help."
    exit;
fi





pmass=0.938
betx=$1
nrj=$2
exn=$3

awk "BEGIN {printf \"sigma is: \";printf sqrt($betx*$exn*$pmass/$nrj);printf \" mm\n\"}"
