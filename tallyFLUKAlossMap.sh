#!/bin/bash

rm collSummary1tallied.dat
for coll in {1..57}
do
    val=$(grep -w $coll collSummaryTemp1.dat | wc -l)
    #echo "Coll "$coll" has "$val" hits"
    echo $coll" "$val >> collSummary1tallied.dat
done

rm collSummary4tallied.dat
for coll in {1..57}
do
    val=$(grep -w $coll collSummaryTemp4.dat | wc -l)
    #echo "Coll "$coll" has "$val" hits"
    echo $coll" "$val >> collSummary4tallied.dat
done   

rm aperLossesTallied.dat
cat aperLossesTemp.dat | sort | uniq -c | awk '{printf $2" "$1"\n"}' > aperLossesTallied.dat

